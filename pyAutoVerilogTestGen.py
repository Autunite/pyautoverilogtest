import argparse, os
import verilog
import pdb

def main():
   parser = argparse.ArgumentParser(prog='testbenchGen',
      description='choose a verilog file to make a testbench for')
      
   #input arg for fileN and path
   parser.add_argument( '-i', '--input', help='choose input file and path', 
                           type=str, required = True)
   
   # Group 1 args
   #output path arg /<path>/
   parser.add_argument( '-op', '--outputPath', 
                           help='select output file path, path must exist', 
                           type=str )
   
   #output fileN arg <text_tb.v>
   parser.add_argument( '-on', '--outputName', 
                           help='choose output file name, i.e v_tb.v', 
                           type=str )
   
   # Group 2 args
   # consider figuring out a way  to combine -on and -op so that you can put
   # in just an file name, or path, or both. Feature creep for later
   # maybe add in a -of that takes in the combined output file name and
   # path. Add a check to make it mutually exclusive with -on and -on and raise
   # the appropriate error when found
   descstr = "specify output file, and name in one arg. "+ \
               "Mutually exclusive with -on and -op args. " + \
               "As with -op output path must exist"
   
   #output fileN and path in one arg
   parser.add_argument( '-of', '--outputFile', 
                           help=descstr, 
                           type=str )
   
   args = parser.parse_args()
   
   
   
   #check that file exists
   try:
      if not os.path.isfile( args.input):
         raise argparse.ArgumentTypeError()
   except argparse.ArgumentTypeError :
      parser.error("File "+args.input+" not found." )
      
   
   #check file extension
   _, ext = os.path.splitext( args.input )
   pdb.set_trace()
   try :
      if ext != '.v':
         raise argparse.ArgumentTypeError
   except argparse.ArgumentTypeError :
      parser.error("Extension for input file is not '.v'")
   
   # file variables
   inFileFullName = os.path.basename( args.input )
   
   #absolute path of the input file
   inFilePath = os.path.dirname(os.path.abspath( args.input ) )
   
   
   # output file name
   outFileName = ""
   
   # output file path
   outFilePath = ""
   
   # output path and name
   outputFullString = ""
   
   
   #this section checks the arguments and fills in the 
   #file I/O variables
   
   #set up the output name without path
   #if -on is not used, then outFileName will default to
   #inFileName+"_tb"+extension
   if ( args.outputPath or args.outputName ) and args.outputFile :
      #put an exception here
      print("args combination error")
      
   
   
   
   if args.outputName :
      outFileName = args.outputName
   else :
      preName = os.path.basename(inFileFullName)
      nameBase , ext = os.path.splitext(preName)
      outFileName = nameBase+"_tb"+ext
      
   #set up output path
   if args.outputPath :
      outFilePath = args.outputPath
      print("Path when -op is used:")
      
   else :
      outFilePath = inFilePath
      print(outFilePath)
   
   ## data processing starts here ##
   print('Input chosen is:')
   print(args.input)
   print('Opening File')
   with open(args.input, 'rb') as f:
      #raw file has all of the line feed and wh spc chars
      rawFile = f.readlines()
      #has no new line chars, but has wh sp chars
   strippedFile = [line.strip() for line in rawFile]
   
   #refined file is a list of lists, with all of the wh sp removed
   # so like:
   # ['//', 'Revision:']
   # ['//', 'Revision', '0.01', '-', 'File', 'Created']
   refinedFile = [line.split() for line in rawFile]
   
   if args.outputName and args.outputPath :
      print "Path and file name chosen :"
      print args.outputPath+args.outputName
   elif args.outputPath :
      print "Chosen path is :"
      print args.outputPath
   elif args.outputName :
      print "Chosen file name is :"
      print args.outputName
   else :
      "Default output directory chosen."
   try:
      if not os.path.isdir( args.outputPath):
         raise argparse.ArgumentTypeError()
   except argparse.ArgumentTypeError :
      print("Path "+args.outputPath+" does not exist." )
      
      
   
   pdb.set_trace()
   print rawFile

if __name__ == '__main__':
   main()