`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    11:37:35 06/07/2018 
// Design Name: 
// Module Name:    multiSpeedClock 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: This module creates a custom clock output for the ADC. The ADC clk ranges from 7-190KHz. And
//		the FIFO clock is always double the frequency of the FIFO clock ranging between 14kHz
//		and 380kHz. Also it has an internal counter so that a specific number of sampling 
//		pulses can be sent out: forever, 1000, 1M, and 10M. 
//
//		400MHz -> 2.5 nanoseconds 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: Due to the nature of integer division the clock frequency will
//		never be exactly the desired output. But at least the clocking speed will be stable
//		The output frequency can be described with the following equation:
//		freqOut = ((input freq Hz)/(desired output frequency Hz)*(input period s))^-1
//		Note: to accurately get the ouput frequency every step before the inversion must
//			be made to take into account for integer division (round down decimals 7.9 -> 7)
//			further rounding errors are caused by the generation of the FIFO clock which further
//			divides the number of clock ticks before the inversion by four to generate a period
//			that is 1/4th the ADC clk
//
//		6/20/2018 GMB: Note yesterday I was dealing with a strange bug, there was an issue with
//		the way the counter was tracked causing either the first or last (currently last) count 
//		to fall short by 50% in terms of timing. As it is it appears to be fixed according to my
//		tests but I shall run it through a greater battery of tests later to see if I can make it
//		reappear. Fixed
//
//		6/22/2018 GMB: New bug, xilinx ISE doesn't recognized dividers for what I am doing, and
//			they are also fairly resource intensive. Lattice may or may not recognize them either,
//			but even if they did they would most likely require several clock cycles to calculate.
//			This would mess with the timing of the clock outputs when switching clock speeds. 
//
//			During lunch I came up with some viable solutions:
//				The easiest would be to hand calculate all 183 values for the various clock speeds.
//			While certainly doable with some RAM, a MUX, and a calculator it would be fairly laborious
//			to do, especially if we switch clocks later.
//				Another option is use an IP core, I signed up for some sites to find open source cores.
//			But they take a couple of days for the registration to go through, plus they would take
//			several clock cycles on start up to do the calculations which requires more logic in the
//			other modules.
//
//			Update: While those solution register I found a new one that will probably work well.
//				I am going to use a generate statement to try to do all the calculations before hand.
//				If possible I am going to use a defparam to define the input clock speed so as I change
//				clock speeds I'll only need to update a single value to make it all possible.	
//
//			17:44 Was running into an error with genvar of an index being out of bounds, which according
//				to websearching was possible to be a xilinx error. Unfortunately when I disabled that code
//				with some webcode I discovered a whole bunch of codependancies between the two Always@
//				statements. Which is unfortunate because it means that a whole bunch of this code will
//				likely need to be refactored, and restructured. So now I am at a loss. The genvar is 
//				critical for generating all of the values for the various clock speeds efficiently
//				and without the potential for human error. It also makes switching between various
//				input clock speeds much easier. I feel that this module should probably be broken down
//				into many submodules. So that is what I am going to do right now, figure out how to make
//				this module into many submodules for easier debugging and synthesis.
//
//		6/25/2018 GMB: I have moved the code over to lattice diamond. Many of my earlier fatal bugs all
//			died in a fire to the compiler. A warning will remain for this module as only memory 
//			7 - 191 are used and memory address 0 is completely unassigned. I originally tried to
//			use a conditional assign statement in the generate block to fill the 0th address with
//			1 but the synthesizer appealed to ignore that while evaluating for divide by zero
//			problems so I cut that out and figured that I would document the warnings. The error
//			a complaint about ADC_clk being assigned in multiple places will need to be dealt with
//			to proceed. An alternative module is partially drawn up but will through away all the 
//			code here to break it up into submodules. Something I would very much like to avoid, so
//			I am going to figure out how to move all the assign statements to the bottom module for
//			ADC_clk by using flags or something else to communicate between the two assign statements.
//			Wish me luck.
//
//		7/3/2018 GMB: Everything broke upon trying to include it in the overall test unit. This was 
//			because reset and loading all depended on the internally generated clock. And if that 
//			clock is not going then nothing will ever be loaded. So it is being rewritten to all
//			be dependent on the fast external clock.
//
//		
//
///////////////////////////////////////////////////////////////////////////////////////////////////////
module multiSpeedClock
	//parameters
	#(
	/*
	This parameter holds the incoming clock frequency in Hz 200x10^6 Hz
	Attempting to use a parameter instead to save on logic
	change this value to represent the frequency of the input clock 
	*/
	parameter in_clk = 200_000_000;
	
	)
	
	//ports
	(
	 //sequence length as specified by the clock commander
     input [1:0] seq_len,
	 
	 //represents whether the clock output should be enabled
	 //or disabled.
	 //	1 = enabled
	 //	0 = disabled
     input sample_state,
	 
	 //represents in KHz the speed that the ADC clock should run at
	 //7 to 190
     input [7:0] clock_spd,
	 
	 
	 //200MHz clk
     input clk_in,
	 
	 //resets everything
	 //sets it to an infinite sampling sequence
	 //with the output disabled
	 //naturally go to 7KHz? so the FIFO clk
	 //runs at 14 KHz?
	 input reset,
	 
	 //loads in setting from the command module
     input load,
	 
	 //ranges from 14-380KHz
	 //always double the frequency of the
	 output reg FIFO_clk,
	 
	 //ranges from 7-190KHz
     output reg ADC_clk 
    );

	//This counter stores the number of samples this module
	//sends out. It is big enough to hold up to a 10M sequence
	//count
	reg [23:0] seq_counter;
	
	//hold the counter the module uses to measure
	//ticks to create a desired frequency
	reg [15:0] clock_counter;
	
	//holds the upper bound that is compared against the clock counter
	//to know when to reset clock_counter and invert the output
	//ram that is 16 bits wide and 256 sections deep
	wire [15:0] clock_comparator[255:0];
	
	//reg that holds the clock comp value
	reg [15:0] clock_comp;
	
	
	//is one if the output is set to sample indefinately
	reg run_indef; 
	
	//represents the frequency in KHz of the FIFO clk
	//probably not needed
	//reg [8:0] FIFO_clk_spd; 
	
	
	//This register holds the frequency of the outgoing clock in Hz, which ranges
	//from 7kHz to 190kHz
	//reg [17:0] out_clk;
	
	//reg goes from 0 1 one depending on whether the
	//output sequence is going or not
	reg run_state;
	
	//this reg is used to theoretically properly sync the adc clock to the
	//fifo clock so you don't get a short clock each time the clock speed 
	//is changed
	reg syncer;
	
	//this assign statement sets the clock comparater to be 1/4 the period of the
	//adc clock, so a FIFO clock can be generated as well
	//this shall be replaced with the genvar statement
	//16 bits
	//assign clock_comparator = (in_clk/out_clk)/4;
	
	genvar gi;
		generate
			for(gi = 0; gi <=255; gi = gi + 1)
			begin : genvalue
				//loads indices 7 to 190 with
				//appropriate values 
				//for clock_comparator
				
				
				if(gi >=1 && gi <= 190) 
					begin
						assign clock_comparator[gi] = ((in_clk/((gi)*1000)))/4; //must be reworked so out_clk ranges from 7-190k
/////////////////-------------------------------^^ Index out of bounds error, why? Dunno, Some sources say that xilinx is bugged
/////////////////Fixed in Lattice					
					end
				else
					begin
						assign clock_comparator[gi] = 1;
					end
					
			end
		endgenerate
	
	//Initial statement matches reset statement
	/*
	initial 
	begin
		seq_counter 	<= 0;
		run_indef		<= 1;
		run_state		<= 0; //////////////////////////////////////////////////////
		out_clk			<= 7000;
		clock_counter	<= 0;
		clock_comp		<= clock_comparator[7];
		FIFO_clk		<= 1;
		ADC_clk			<= 1;
		syncer			<= 0;
	end
	*/
	
	//attempt to unify everything under a single clock
	always@(posedge clk_in)
		begin
			
			//if reset asserted
			if(reset)
				begin
					///////////////////////////////////////////////////////////////////
					//changing to run indefinately for slow test of packet loss over weekend
					seq_counter 	<= 1000;
					run_indef		<= 0;
					run_state		<= 1;
					clock_counter	<= 0;
					FIFO_clk		<= 1;
					ADC_clk			<= 1;
					clock_comp		<= clock_comparator[1];
					syncer			<= 0;
				end
				
			//if load is asserted
			else if(load)
				begin
					clock_comp		<= clock_comparator[clock_spd];
					run_state		<= sample_state;
					FIFO_clk		<= 1;
					ADC_clk			<= 1;
					syncer			<= 0;
					seq_counter		<= 0; /////////////
					
					//set sequence counter
					//and run indef
					
					//if seq_len == 0 then
					//set sequence counter to 0
					//and run indef to 1
					if(seq_len == 0)
						begin
							seq_counter	<= 0;
							run_indef	<= 1;
						end
						
					//if 1 then do 1000 samples
					else if(seq_len == 1)
						begin
							seq_counter	<= 1000;
							run_indef	<= 0;
						end
						
					//if two do a million samples
					else if(seq_len == 2)
						begin
							seq_counter	<= 1_000_000;
							run_indef	<= 0;
						end
						
					//else do ten million samples
					else
						begin
							seq_counter	<= 10_000_000;
							run_indef	<= 0;
						end
						
				end
				
			//else do the clock counter
			//clock_counter
			//clock_comp
			//seq_counter
			//FIFO_clk
			//ADC_clk
			//syncer
			else
				begin
					
					//if run_state is 1
					if(run_state ==1)
						begin
							
						//if run_indef == 1
						if(run_indef == 1)
							begin
								
								//if clock_counter == clock_comp
								if(clock_counter == clock_comp)
									begin
										clock_counter 	<= 0;
										FIFO_clk		<= ~FIFO_clk;
										
										//syncer is built in so that
										//the timing is maintained consistently
										//for the first and last sample
										if(syncer == 1)
											begin
												ADC_clk	<= ~ADC_clk;
												syncer	<= 0;
											end
											
										else
											begin
												syncer <= 1;
											end
									end
								else
									begin
										clock_counter <= clock_counter+1;
									end
							end
							
						//if run_indef == 0
						else
							begin
								
								//if sequence counter == 0
								if(seq_counter == 0)
									begin
										run_state <= 0;
									end
									
								else
									begin
										
										//if clock_counter == clock_comp
										if(clock_counter == clock_comp)
											begin
												//counter rolls around when it is
												//equal to the clock comparator
												clock_counter 	<= 0;
												FIFO_clk		<= ~FIFO_clk;
										
												//syncher does it's thing to maintain
												//the adc clock's timing, at the first
												//and last clock
												if(syncer == 1)
													begin
														ADC_clk	<= ~ADC_clk;
														syncer	<= 0;
													
														//deal with seq_counter
														if(ADC_clk == 0 && FIFO_clk == 0 )
															begin
																seq_counter <= seq_counter - 1;
															end
													end
													
												else
													begin
														syncer <= 1;
													end
											end
										//each time increment the clock counter
										//when none of the other conditions apply
										else
											begin
												clock_counter <= clock_counter+1;
											end
									end
							end
						end
						
					//if run_state is 0
					//do nothing
					else
						begin
							
						end
				end
		end
		
endmodule
