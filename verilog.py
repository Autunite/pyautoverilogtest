#this file contains the various structures of verilog

#This class defines the size of the indent
#And has a function that returns the depth 
#  of indent based on input int

#helper functions

#classes for various verilog constructs
class Indent:
   def __init__(self, tab_size=3):
         self.tab_size = tab_size
         self.tab_lit = self._build_tab(tab_size)
         
   #check if 0 is returned for 0 being inputted
   def _build_tab(self, tab_size ):
      sp = ' '
      ret_var = ''
      for x in range(tab_size):
         ret_var += sp
      return ret_var
   
   def indent(self, num_tabs=1):
      tab = ''
      for x in range(num_tabs):
         tab += self.tab_lit
      return tab


# all classes below inherit from this.
class Verilog_Construct:
   def __init__(self, indent_depth, cr_num):
      self.indent_depth = indent_depth
      self.creation_counter =creation_counter
      
   def __str__(self):
      pass

class Time_Scale(Verilog_Construct):
   def __init__(self):
      pass
      
   def __str__(self):
      pass

class Port(Verilog_Construct):
   def __init__(self, direction, io_type, bus_size, name):
      self.direction = direction
      self.io_type = io_type
      self.bus_size = bus_size
      self.name = name
      
   def __str__(self):
      pass

class Port_List(Verilog_Construct):
   def __init__(self):
      pass
   
   def __str__(self):
      pass
   
class Parameter(Verilog_Construct):
   def __init__(self):
      pass
   
   def __str__(self):
      pass
      
class Parameter_List(Verilog_Construct):
   def __init__(self):
      pass
   
   def __str__(self):
      pass
      
class Module(Verilog_Construct):
   def __init__(self, name, paramList, portList):
      pass
      
   def __str__(self):
      pass
      
class Verilog_File(Verilog_Construct):
   def __init__( self, **kwargs ):
      self.time_scale = kwargs['time_scale']
      self.intro_template = kwargs['intro_template']
      self.param_list = kwargs['param_list']
      self.port_list = kwargs['port_list']
      
   def __str__(self):
      pass